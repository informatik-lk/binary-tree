import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class Node {
    private Node left, right;
    private int content;

    public Node(int data) {
        this.left = null;
        this.right = null;
        this.content = data;
    }

    public int getData() {
        return this.content;
    }

    public Node getLeft() {
        return this.left;
    }

    public Node getRight() {
        return this.right;
    }

    public void setLeft(Node data) {
        this.left = data;
    }

    public void setRight(Node data) {
        this.right = data;
    }
}