
public class Main {
    public static void main(String[] args) {
        Tree t = new Tree();

        t.insert(17);
        t.insert(13);
        t.insert(19);
        t.insert(12);
        t.insert(0);

        System.out.println(t.isBalanced());

        t.print();
    }
}
