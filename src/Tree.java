import java.util.ArrayList;
import java.util.Objects;

public class Tree {
    private Node root;

    public Tree() {
        this.root = null;
    }

    public void insert(int data) {
        this.root = this.insertHelper(root, data);
    }

    public Node getRoot() { return this.root; }

    private Node insertHelper(Node node, int data) {
        if (Objects.isNull(node)) {
            node = new Node(data);
        } else {
            if (data < node.getData()) {
                node.setLeft(this.insertHelper(node.getLeft(), data));
            } else {
                node.setRight(this.insertHelper(node.getRight(), data));
            }
        }
        return node;
    }

    public Object[] traverseInOrder() {
        ArrayList<Integer> list = new ArrayList<Integer>();

        this.inOrderHelper(this.root, list);

        return list.toArray();
    }

    private ArrayList<Integer> inOrderHelper(Node node, ArrayList<Integer> list) {
        if (Objects.isNull(node)) {
            return list;
        }

        this.inOrderHelper(node.getLeft(), list);

        list.add(node.getData());

        this.inOrderHelper(node.getRight(), list);

        return list;
    }

    public Object[] traversePreOrder() {
        ArrayList<Integer> list = new ArrayList<Integer>();

        this.preOrderHelper(this.root, list);

        return list.toArray();
    }

    private ArrayList<Integer> preOrderHelper(Node node, ArrayList<Integer> list) {
        if (Objects.isNull(node)) {
            return list;
        }

        list.add(node.getData());

        this.preOrderHelper(node.getLeft(), list);

        this.preOrderHelper(node.getRight(), list);

        return list;
    }

    public Object[] traversePostOrder() {
        ArrayList<Integer> list = new ArrayList<Integer>();

        this.postOrderHelper(this.root, list);

        return list.toArray();
    }

    private ArrayList<Integer> postOrderHelper(Node node, ArrayList<Integer> list) {
        if (Objects.isNull(node)) {
            return list;
        }

        this.postOrderHelper(node.getLeft(), list);

        this.postOrderHelper(node.getRight(), list);

        list.add(node.getData());

        return list;
    }

    public int findHeight(Node node) {
        if (Objects.isNull(node))
            return 0;

        return 1
            + Math.max(this.findHeight(node.getLeft()),
                this.findHeight(node.getRight()));
    }

    public boolean isBalanced() {
        return this.balancedHelper(root);
    }

    private boolean balancedHelper(Node node) {
        int leftHeight, rightHeight;

        if (node == null)
            return true;

        leftHeight = this.findHeight(node.getLeft());
        rightHeight = this.findHeight(node.getRight());

        if (Math.abs(leftHeight - rightHeight) <= 1 && this.balancedHelper(node.getLeft()) && this.balancedHelper(node.getRight()))
            return true;

        return false;
    }

    public void remove(Node node) throws Exception {
        if (Objects.isNull(node.getLeft()) && Objects.isNull(node.getRight())) {
            // Node has no children => Leaf Node
            Node parent = this.findParent(node);

            if (parent.getLeft() == node) {
                parent.setLeft(null);
            } else if (parent.getRight() == node) {
                parent.setRight(null);
            } else {
                throw new Exception("Errorrrrr", null);
            }
        } else if (Objects.isNull(node.getLeft()) ^ Objects.isNull(node.getRight()))  {
            // Node has just 1 child
        }
    }

    private Node findParent(Node n) {
        Node current;

        current = this.root;

        while (!(current.getLeft() == n || current.getRight() == n)) {
            current = n.getData() < this.root.getData() ? current.getLeft() : current.getRight();
        }

        return current;
    }

    public Node rotateWithLeftChild(Node n2)
    {
        Node n1 = n2.getLeft();
        n2.setLeft(n1.getRight());
        n1.setRight(n2);
        return n1;
    }

    public Node rotateWithRightChild(Node n1)
    {
        Node n2 = n1.getRight();
        n1.setRight(n2.getLeft());
        n2.setLeft(n1);
        return n2;
    }

    public Node doubleRotateWithLeftChild(Node k3)
    {
        k3.setLeft(rotateWithRightChild(k3.getLeft()));
        return rotateWithLeftChild(k3);
    }

    public Node doubleRotateWithRightChild(Node n1)
    {
        n1.setRight(rotateWithLeftChild(n1.getRight()));
        return rotateWithRightChild(n1);
    }

    public void print() {
        print(this.root, 0);
    }
      
    public static void print(Node n, int level){
        if(n==null)
            return;
        print(n.getRight(), level+1);
        if(level!=0){
            for(int i=0;i<level-1;i++)
            System.out.print("|\t");
            System.out.println("|-------"+n.getData());
        }
        else
            System.out.println(n.getData());
        print(n.getLeft(), level+1);
    }
}
